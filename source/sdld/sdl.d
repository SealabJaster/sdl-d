﻿module sdld.sdl;

public
{
    import std.string    : fromStringz;
    import std.exception : assumeUnique;

    import derelict.sdl2.functions, derelict.sdl2.sdl, derelict.sdl2.types;
}

class SDLException : Exception
{
    /++
     + Constructs a new SDLException that throws with an error message from SDL_GetError.
     + ++/
    this()
    {
        super(SDL_GetError().fromStringz().assumeUnique());
    }

    /++
     + Constructs a new SDLException that throws with the given error message.
     + 
     + Parameters:
     +  message = The error message.
     + ++/
    this(string message)
    {
        super(message);
    }
}