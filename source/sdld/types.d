﻿module sdld.types;

private
{
    import sdld.sdl;
}

/++
 + A struct that holds 2 of something.
 + ++/
struct Vector2(T)
{
    // TODO: Overload arithmetic operators.

    public
    {
        /// The X of the vector.
        T x;

        /// The Y of the vector.
        T y;

        static if(is(T == int))
        {
            /++
             + Converts the vector2i into an SDL_Point.
             + ++/
            @property @nogc @safe
            SDL_Point asPoint() nothrow
            {
                return SDL_Point(this.x, this.y);
            }
        }
    }
}
alias Vector2i = Vector2!int;
alias Vector2u = Vector2!uint;
alias Vector2f = Vector2!float;
alias Vector2d = Vector2!double;