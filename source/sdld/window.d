﻿module sdld.window;

private
{
    import std.string : toStringz, fromStringz;
    import std.exception;

    import sdld.sdl, sdld.types;
}

/// Used for Window.fullscreenMode
enum FullscreenMode
{
    /// Proper fullscreen.
    Fullscreen = SDL_WINDOW_FULLSCREEN,

    /// Full screen mode that takes up the size of the desktop. "Fake" fullscreen.
    WindowedFullscreen = SDL_WINDOW_FULLSCREEN_DESKTOP,

    /// No fullscreen.
    Normal = 0
}

/++
 + Contains the position, size, and flags of a window.
 + ++/
struct WindowStyle
{
    /// The X position of the window
    int x = SDL_WINDOWPOS_CENTERED;

    /// The Y position of the window
    int y = SDL_WINDOWPOS_CENTERED;

    /// The width of the window.
    int w;

    /// The height of the window.
    int h;

    /// 0 or more flags of $(B SDL_WINDOW_XXX).
    uint flags;
}

/++
 + Contains an SDL_DisplayMode.
 + It mostly exists so I can add some static functions later. E.G DisplayMode.getCurrent(int displayIndex)
 + ++/
struct DisplayMode
{
    // TODO: Look at the related functions, and add them in. https://wiki.libsdl.org/SDL_DisplayMode
    alias handle this;

    public
    {
        /++
         + The DisplayMode's internal SDL_DIsplayMode.
         + 
         + Fields:
         +  uint format - An enum of $(B SDL_PIXELFORMAT_XXX) values.
         +  int w - Width
         +  int h - Height
         +  int refresh_rate - Refresh rate const Hz. 0 for unspecified.
         + ++/
        SDL_DisplayMode handle;

        /++
         + Get a pointer to the DisplayMode's handle.
         + ++/
        @property @safe @nogc
        inout(SDL_DisplayMode*) handleRef() nothrow inout
        {
            return &this.handle;
        }
    }
}

/++
 + A wrapper around an SDL_Window*
 + Note, this struct does not handle destroying the window.
 + ++/
struct Window
{
    // TODO: Add any functions/structs/whatever dealing with displays. E.g. SDL_GetWindowDisplayIndex. SDL_GetClosestDisplayMode(int displayIndex), etc.
    private
    {
        SDL_Window* _handle;
    }

    public
    {
        /++
         + Constructs a Window around the given SDL_Window*
         + 
         + Parameters:
         +  window = The window handle.
         + ++/
        @safe @nogc
        this(SDL_Window* window) nothrow
        {
            this._handle = window;
        }

        /++
         + An @nogc version of the window's constructor.
         + Manual error checking must be done by checking $(B Window.handle) if needed.
         + 
         + Parameters:
         +  title = The window's title.
         +  WindowStyle = The window's style
         + ++/
        @nogc
        this(const char* title, const WindowStyle style) nothrow
        {
            this._handle = SDL_CreateWindow(title, style.x, style.y, style.w, style.h, style.flags);
        }

        /++
         + Constructs a new SDL_Window* and wraps around it.
         + 
         + Parameters:
         +  title = The window's title.
         +  style = The window's style.
         + 
         + Throws:
         +  SDLException if the window couldn't be created.
         + ++/
        this(const string title, const WindowStyle style)
        {
            this(title.toStringz(), style);
            enforce(this._handle !is null, new SDLException());
        }

        /++
         + Calls SDL_DestroyWindow on the Window's handle.
         + The handle is then set to null.
         + ++/
        void destroy()
        {
            if(this._handle !is null)
            {
                SDL_DestroyWindow(this._handle);
                this._handle = null;
            }
        }

        /++
         + Allows you to store an abitrary data pointer with the window.
         + 
         + Parameters:
         +  name = The name of the data
         +  data = The pointer to store
         + 
         + Returns:
         +  The previous pointer stored that was associated with $(B name).
         + ++/
        @nogc
        T* setData(T)(const char* name, T* data) nothrow
        {
            return SDL_SetWindowData(this.handle, name, cast(void*)data);
        }

        /// Ditto
        T* setData(T)(const string name, T* data)
        {
            return this.setData(name.toStringz(), data);
        }

        /++
         + Allows you to retrieve an arbitrary data pointer from the window.
         + 
         + Parameters:
         +  name = The name of the pointer to get.
         + 
         + Returns:
         +  The pointer associated with $(B name).
         + ++/
        @nogc
        T* getData(T)(const char* name) nothrow
        {
            return cast(T*)SDL_GetWindowData(this.handle, name);
        }

        /// Ditto
        T* getData(T)(const string name)
        {
            return this.getData!T(name.toStringz());
        }

        /++
         + Hides the window.
         + ++/
        @nogc
        void hide() nothrow
        {
            SDL_HideWindow(this.handle);
        }

        /++
         + Shows the window.
         + ++/
        @nogc
        void show() nothrow
        {
            SDL_ShowWindow(this.handle);
        }

        /++
         + Maximises the window.
         + ++/
        @nogc
        void maximise() nothrow
        {
            SDL_MaximizeWindow(this.handle);
        }
        alias maximize = maximise;

        /++
         + Minimises the window.
         + ++/
        @nogc
        void minimise() nothrow
        {
            SDL_MinimizeWindow(this.handle);
        }
        alias minimize = minimise;

        /++
         + Raises the window above other windows, and sets the input focus.
         + ++/
        @nogc
        void raise() nothrow
        {
            SDL_RaiseWindow(this.handle);
        }

        /++
         + Undoes the effect of maximise/minimise
         + ++/
        @nogc
        void restore() nothrow
        {
            SDL_RestoreWindow(this.handle);
        }

        /++
         + Get whether the window has a border or not.
         + ++/
        @property @nogc
        bool hasBorder() nothrow
        {
            return !(this.flags & SDL_WINDOW_BORDERLESS);
        }

        /++
         + Set whether the window has a border or not.
         + ++/
        @property @nogc
        void hasBorder(bool border) nothrow
        {
            SDL_SetWindowBordered(this.handle, (border) ? SDL_TRUE : SDL_FALSE);
        }

        /++
         + Set the fullscreen mode of the window.
         + ++/
        @property
        void fullscreenMode(FullscreenMode mode)
        {
            enforce(SDL_SetWindowFullscreen(this.handle, cast(uint)mode) == 0, new SDLException());
        }

        /++
         + Set whether the mouse is confined to the window.
         + ++/
        @property @nogc
        void grabMouse(bool toGrabOrNotToGrab) nothrow
        {
            SDL_SetWindowGrab(this.handle, toGrabOrNotToGrab);
        }

        /++
         + Get whether the mouse is confined to the window.
         + ++/
        @property @nogc
        bool grabMouse() nothrow
        {
            return (SDL_GetWindowGrab(this.handle) == SDL_TRUE) ? true : false;
        }

        /++
         + Set the window's icon.
         + ++/
        @property @nogc
        void icon(SDL_Surface* icon) nothrow
        {
            SDL_SetWindowIcon(this.handle, icon);
        }

        /++
         + Get the Window's display mode.
         + ++/
        @property @nogc
        DisplayMode displayMode() nothrow
        {
            DisplayMode toReturn;
            SDL_GetWindowDisplayMode(this.handle, toReturn.handleRef);

            return toReturn;
        }

        /++
         + Set the Window's display mode.
         + Passing in just a "DisplayMode()" will make the window use the window's dimensions and the desktop's format and refresh rate.
         + Only works if the Window is in fullscreen mode.
         + ++/
        @property
        void displayMode(const DisplayMode mode)
        {
            enforce(SDL_SetWindowDisplayMode(this.handle, mode.handleRef) == 0, new SDLException());
        }

        /++
         + Get the Window's flags. (An enum of $(B SDL_WINDOW_XXX) values)
         + ++/
        @property @nogc
        uint flags() nothrow
        {
            return SDL_GetWindowFlags(this.handle);
        }

        /++
         + Get whether the Window is currently grabbing input.
         + ++/
        @property @nogc
        bool isInputGrabbed() nothrow
        {
            return (SDL_GetWindowGrab(this.handle) == SDL_TRUE);
        }

        /++
         + Get the Window's numeric id. For logging purposes.
         + ++/
        @property @nogc
        uint id() nothrow
        {
            return SDL_GetWindowID(this.handle);
        }

        /++
         + Get the maximum size of the window's client area.
         + ++/
        @property @nogc
        Vector2i maximumSize() nothrow
        {
            mixin(getWindowVector2i!"SDL_GetWindowMaximumSize");
        }

        /++
         + Set the maximum size of the window's client area.
         + ++/
        @property @nogc
        void maximumSize(Vector2i size) nothrow
        {
            SDL_SetWindowMaximumSize(this.handle, size.x, size.y);
        }

        /++
         + Get the minimum size of the window's client area.
         + ++/
        @property @nogc
        Vector2i minimumSize() nothrow
        {
            mixin(getWindowVector2i!"SDL_GetWindowMinimumSize");
        }

        /++
         + Set the minimum size of the window's client area.
         + ++/
        @property @nogc
        void minimumSize(Vector2i size) nothrow
        {
            SDL_SetWindowMinimumSize(this.handle, size.x, size.y);
        }

        /++
         + Get the position of the window.
         + ++/
        @property @nogc
        Vector2i position() nothrow
        {
            mixin(getWindowVector2i!"SDL_GetWindowPosition");
        }

        /++
         + Set the position of the window.
         + ++/
        @property @nogc
        void position(Vector2i pos) nothrow
        {
            SDL_SetWindowPosition(this.handle, pos.x, pos.y);
        }

        /++
         + Get the window's size.
         + TODO: A function for SDL_GL_GetDrawableSize. For when the window is created with ALLOW_HIGHDPI
         + ++/
        @property @nogc
        Vector2i size() nothrow
        {
            mixin(getWindowVector2i!"SDL_GetWindowSize");
        }

        /++
         + Set the window's size.
         + ++/
        @property @nogc
        void size(Vector2i _size) nothrow
        {
            SDL_SetWindowSize(this.handle, _size.x, _size.y);
        }

        /++
         + Get the window's title.
         + ++/
        @property @nogc
        string title() nothrow
        {
            // I assume I don't need to free the char*... If I do then, bleh..
            return SDL_GetWindowTitle(this.handle).fromStringz().assumeUnique();
        }

        /++
         + Set the window's title.
         + ++/
        @property
        void title(string newTitle) nothrow
        {
            SDL_SetWindowTitle(this.handle, newTitle.toStringz());
        }

        /// Ditto
        @property @nogc
        void title(const char* newtitle) nothrow
        {
            SDL_SetWindowTitle(this.handle, newtitle);
        }

        /++
         + Get driver specific information about the window.
         + 
         + Throws:
         +  SDLException if the information couldn't be retrieved.
         + ++/
        @property
        SDL_SysWMinfo info()
        {
            SDL_SysWMinfo data;
            enforce(SDL_GetWindowWMInfo(this.handle, &data) == SDL_TRUE, new SDLException());

            return data;
        }

        /++
         + Get the Window's handle.
         + ++/
        @property @safe @nogc
        SDL_Window* handle() pure nothrow
        {
            return this._handle;
        }
    }
}

private
{
    /++
     + A mixin string to get a Vector2i from one of the SDL_GetWindowX functions.
     + ++/
    string getWindowVector2i(string funct)()
    {
        return "Vector2i toReturn; "~funct~"(this.handle, &toReturn.x, &toReturn.y); return toReturn;";
    }
}